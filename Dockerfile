FROM docker:19.03.13

LABEL maintainer=pinchengshih@kklab.com

# init
RUN apk add curl zip git

# Packer
RUN curl -L https://releases.hashicorp.com/packer/1.7.10/packer_1.7.10_linux_amd64.zip -o /tmp/packer.zip && \
    unzip /tmp/packer.zip -d /usr/local/bin/

# kubectl
# https://github.com/txn2/docker-kubectl/blob/master/Dockerfile
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.0/bin/linux/amd64/kubectl && \
    chmod 755 kubectl && \
    mv kubectl /usr/local/bin/kubectl

# Helm
RUN curl -LO https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz && \
    tar zxvf helm-v3.8.0-linux-amd64.tar.gz && \
    chmod 755 linux-amd64/helm && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf linux-amd64 helm-v3.8.0-linux-amd64.tar.gz

# helm secrets
RUN helm plugin install https://github.com/jkroepke/helm-secrets --version v3.12.0

# python3
RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache

# Ansible and awscli
# awscliv2 is not available on alpine
RUN apk add --no-cache libffi-dev build-base gcc openssl-dev openssh-client python3-dev && \
    pip3 install wheel
RUN CRYPTOGRAPHY_DONT_BUILD_RUST=1 pip3 install ansible==4.0.0 awscli boto==2.49.0 boto3==1.20.51
COPY ansible /etc/ansible

